<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AdamBrett\ShellWrapper\Command;
use AdamBrett\ShellWrapper\Command\Param;
use AdamBrett\ShellWrapper\Runners\Exec;

class DeployController extends Controller
{
    public function index()
    {
        chdir('/home/mgl/almerinet.org');
        $shell = new Exec();
        $command = new Command('git pull');
        //$command->addParam(new Param('Hello World'));
        $output = $shell->run($command);
        return view('deploy.index', ['output' => $output]);

    }
}
